package taller.test;

import java.util.Random;

import junit.framework.TestCase;
import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.BubbleSortTeam;
import taller.mundo.teams.InsertionSortTeam;
import taller.mundo.teams.MergeSortTeam;
import taller.mundo.teams.QuickSortTeam;
import taller.mundo.teams.SelectionSortTeam;
import taller.mundo.teams.ShellSortTeam;
import taller.mundo.teams.TimSortTeam;

public class MainTest extends TestCase{
   
		private Comparable[] lista;
		private BubbleSortTeam burbuja;
		private InsertionSortTeam insertion;
		private SelectionSortTeam selection;
		private MergeSortTeam merge;
		private QuickSortTeam quick;
		private ShellSortTeam shell;
		private TimSortTeam tim;

		public void setUp()
		{
			selection = new SelectionSortTeam();
			burbuja = new BubbleSortTeam();
			insertion = new InsertionSortTeam();
			merge = new MergeSortTeam();
			quick = new QuickSortTeam();
			shell = new ShellSortTeam();
			tim = new TimSortTeam();
			
			lista = new Comparable[15];
			for (int i = 0; i < 15; i++) 
			{
				lista[i] = i+(i+5);
			}
		}

		public void testOrdenarAscendenteInsertionSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			insertion.sort(lista, TipoOrdenamiento.ASCENDENTE);


			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue("Ordenamiento ascendente por InsertionSortTeam es incorrecto", v1.compareTo(v2) <= 0);
			}
			assertEquals("Se borraron elementos en InsertionSortTeam ", tamPrevio, lista.length);

		}
		public void testOrdenarDescendenteInsertionSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			insertion.sort(lista, TipoOrdenamiento.DESCENDENTE);

			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue( "Ordenamiento ascendente por InsertionSortTeam es incorrecto", v1.compareTo(v2) >= 0);
			}
			assertEquals("Se borraron elementos en InsertionSortTeam ", tamPrevio, lista.length);
		}

		public void testOrdenarAscendenteSelectionSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			Comparable[] test = selection.sort(lista, TipoOrdenamiento.ASCENDENTE);

			for (int i = 0; i < test.length-1; i++) {
				Comparable v1 = test[i];
				Comparable v2 = test[i+1];
				assertTrue("Ordenamiento ascendente por SelectionSortTeam es incorrecto", v1.compareTo(v2) <= 0);
			}
			assertEquals("Se borraron elementos en SelectionSortTeam ", tamPrevio, test.length);

		}
		public void testOrdenarDescendenteSelectionSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			selection.sort(lista, TipoOrdenamiento.DESCENDENTE);

			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue( "Ordenamiento ascendente por SelectionSortTeam es incorrecto", v1.compareTo(v2) >= 0);
			}
			assertEquals("Se borraron elementos en SelectionSortTeam ", tamPrevio, lista.length);
		}
		public void testOrdenarAscendenteMergeSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			merge.sort(lista, TipoOrdenamiento.ASCENDENTE);


			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue("Ordenamiento ascendente por MergeSortTeam es incorrecto", v1.compareTo(v2) <= 0);
			}
			assertEquals("Se borraron elementos en MergeSortTeam ", tamPrevio, lista.length);

		}
		public void testOrdenarDescendenteMergeSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			merge.sort(lista, TipoOrdenamiento.DESCENDENTE);

			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue( "Ordenamiento ascendente por MergeSortTeam es incorrecto", v1.compareTo(v2) >= 0);
			}
			assertEquals("Se borraron elementos en MergeSortTeam ", tamPrevio, lista.length);
		}
		public void testOrdenarAscendenteQuickSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			quick.sort(lista, TipoOrdenamiento.ASCENDENTE);


			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue("Ordenamiento ascendente por QuickSortTeam es incorrecto", v1.compareTo(v2) <= 0);
			}
			assertEquals("Se borraron elementos en QuickSortTeam ", tamPrevio, lista.length);

		}
		public void testOrdenarDescendenteQuickSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			quick.sort(lista, TipoOrdenamiento.DESCENDENTE);

			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue( "Ordenamiento ascendente por QuickSortTeam es incorrecto", v1.compareTo(v2) >= 0);
			}
			assertEquals("Se borraron elementos en QuickSortTeam ", tamPrevio, lista.length);
		}
		public void testOrdenarAscendenteShellSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			shell.sort(lista, TipoOrdenamiento.ASCENDENTE);

			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue("Ordenamiento ascendente por ShellSortTeam es incorrecto", v1.compareTo(v2) <= 0);
			}
			assertEquals("Se borraron elementos en ShellSortTeam ", tamPrevio, lista.length);

		}
		public void testOrdenarDescendenteShellSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			shell.sort(lista, TipoOrdenamiento.DESCENDENTE);

			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue( "Ordenamiento ascendente por ShellSortTeam es incorrecto", v1.compareTo(v2) >= 0);
			}
			assertEquals("Se borraron elementos en ShellSortTeam ", tamPrevio, lista.length);
		}
		public void testOrdenarAscendenteTimSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			tim.sort(lista, TipoOrdenamiento.ASCENDENTE);


			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue("Ordenamiento ascendente por TimSortTeam es incorrecto", v1.compareTo(v2) <= 0);
			}
			assertEquals("Se borraron elementos en TimSortTeam ", tamPrevio, lista.length);

		}
		public void testOrdenarDescendenteTimSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			tim.sort(lista, TipoOrdenamiento.DESCENDENTE);

			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue( "Ordenamiento ascendente por TimSortTeam es incorrecto", v1.compareTo(v2) >= 0);
			}
			assertEquals("Se borraron elementos en TimSortTeam ", tamPrevio, lista.length);
		}
		public void testOrdenarAscendenteBubbleSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			burbuja.sort(lista, TipoOrdenamiento.ASCENDENTE);


			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue("Ordenamiento ascendente por BubbleSortTeam es incorrecto", v1.compareTo(v2) <= 0);
			}
			assertEquals("Se borraron elementos en BubbleSortTeam ", tamPrevio, lista.length);

		}
		public void testOrdenarDescendenteBubbleSortTeam( )
		{
			int tamPrevio = lista.length;
			shuffle(lista);
			burbuja.sort(lista, TipoOrdenamiento.DESCENDENTE);

			for (int i = 0; i < lista.length-1; i++) {
				Comparable v1 = lista[i];
				Comparable v2 = lista[i+1];
				assertTrue( "Ordenamiento ascendente por BubbleSortTeam es incorrecto", v1.compareTo(v2) >= 0);
			}
			assertEquals("Se borraron elementos en BubbleSortTeam ", tamPrevio, lista.length);
		}

		public void shuffle(Comparable [] a){
			for(int i=0;i<a.length;i++)
				swap(a,i,getRandom(i,a.length-1));
		}

		private int getRandom(int min, int max){
			Random rnd = new Random();
			return min + rnd.nextInt(max-min+1);
		}

		private void swap(Comparable [] a, int i, int j){
			Comparable temp = a[i];
			a[i]=a[j];
			a[j]=temp;
		}
	}