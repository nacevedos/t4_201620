package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{


	public MergeSortTeam()
	{
		super("Merge sort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		return merge_sort(lista, orden);
	}


	private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		// Trabajo en Clase
		if (lista.length<=1)
			return lista;

		Comparable [] izquierda=new Comparable[(lista.length/2)] ;
		Comparable [] derecha=new Comparable[lista.length-izquierda.length] ;
		System.arraycopy(lista, 0, izquierda,0, izquierda.length);
		System.arraycopy(lista, (izquierda.length), derecha, 0, derecha.length);

		Comparable [] iz=merge_sort(izquierda, orden);
		Comparable [] der=merge_sort(derecha, orden);

		return merge(iz, der, orden);

	}

	private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
	{
		// Trabajo en Clase 

		//try just as the ppt
		Comparable [] aux= new Comparable[izquierda.length+derecha.length];
		int merge=0;
		int j=0;
		if(orden.equals(TipoOrdenamiento.ASCENDENTE))
		{
		for(int i=0; i<izquierda.length;i++)
		{

			if (j>=derecha.length)
			{
				aux[merge]=izquierda[i];
			}
			else if (izquierda[i].compareTo(derecha[j])>0)
			{
				aux[merge]=derecha[j];
				j++;
				i--;
			}

		
			else
			{
				aux[merge]=izquierda[i];
			}

			merge++;
		}
		int prueba= aux.length;
		if (merge<aux.length)
		{
			while(j<derecha.length)
			{
				aux[merge]=derecha[j];
				j++;
				merge++;
			}
		}

		return aux;
		}
		else 
		{
			for(int i=0; i<izquierda.length;i++)
			{

				if (j>=derecha.length)
				{
					aux[merge]=izquierda[i];
				}
				else if (izquierda[i].compareTo(derecha[j])<0)
				{
					aux[merge]=derecha[j];
					j++;
					i--;
				}

			
				else
				{
					aux[merge]=izquierda[i];
				}

				merge++;
			}
			int prueba= aux.length;
			if (merge<aux.length)
			{
				while(j<derecha.length)
				{
					aux[merge]=derecha[j];
					j++;
					merge++;
				}
			}

			return aux;
		}

	}


}
