package taller.mundo.teams;


import java.util.Arrays;
import java.util.Random;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class QuickSortTeam extends AlgorithmTeam
{

	private static Random random = new Random();

	public QuickSortTeam()
	{
		super("Quicksort");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		quickSort(list, 0, list.length-1, orden);
		return list;
	}

	private static void quickSort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
	{
		// Trabajo en Clase
		if(inicio<=fin)return;
		int j = particion(lista, inicio, fin, orden);
		quickSort(lista, inicio, j-1, orden);
		quickSort(lista, j+1, fin, orden);



	}

	private static int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
	{
		int i = inicio;
		int f = fin+1;
		
		int ele=eleccionPivote(inicio, fin);
		Comparable temp = lista[ele];
		lista[ele] = inicio;
		lista[inicio] = temp;
		Comparable pivot=lista[inicio];

		
		if(orden.equals(TipoOrdenamiento.ASCENDENTE))
		{

		while(true)
		{
			while (lista[i++].compareTo(pivot) < 0)
			{
				
				if ( i==fin)
					break;

			}

			while(lista[--f].compareTo(pivot) > 0)
			{
				
				if (f==inicio)
					break;

			}
			if (i >= f)
			{
				break;
			}


			Comparable temp3 = lista[i];
			lista[i] = lista[f];
			lista[f] = temp3;



		}
		Comparable temp2 = lista[f];
		lista[f] = lista[inicio];
		lista[inicio] = temp2;
		
		return f;
	}
		else 
		{
			while(true)
			{
				while (lista[i++].compareTo(pivot) > 0)
				{
						
					if ( i==fin)
						break;

				}

				while(lista[--f].compareTo(pivot) < 0)
				{
					
					if (f==inicio)
						break;

				}
				if (i >= f)
				{
					break;
				}


				Comparable temp2 = lista[i];
				lista[i] = lista[f];
				lista[f] = temp2;



			}
			Comparable temp2 = lista[f];
			lista[f] = lista[inicio];
			lista[inicio] = temp2;
			
			return f;
		}
	}
	

	private static int eleccionPivote(int inicio, int fin)
	{
		/**
           Este procedimiento realiza la elecci�n de un �ndice que corresponde al pivote res-
           pecto al cual se realizar�  la partici�n de la lista. Se recomienda escoger el ele-
           mento que se encuentra en la mitad, o de forma aleatoria entre los �ndices [inicio, fin).
		 **/
		// Trabajo en Clase

		return randInt(inicio, fin);
	}

	/**
      Retorna un número aleatorio que se encuentra en el intervalo [min, max]; inclusivo.
      @param min, índice inicial del intervalo.
      @param max, índice final del intervalo.
      @return Un número aleatorio en el intervalo [min, max].
	 **/
	public static int randInt(int min, int max) 
	{
		int randomNum = random.nextInt((max - min) + 1) + min;
		return randomNum;
	}
	// Trabajo en Clase

}
