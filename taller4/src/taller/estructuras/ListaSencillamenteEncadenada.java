package taller.estructuras;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Clase que representa una lista sencillamente encadenada
 * @author alvar-go
 * @param <T>
 */
public class ListaSencillamenteEncadenada<T extends IdentificadoUnicamente> extends ListaEncadenadaAbstracta<T>
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construye una lista vacia
     * post: se ha inicializado el primer nodo en null
     */
    public ListaSencillamenteEncadenada()
    {
        primero = null;
    }
    
    /**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
    public ListaSencillamenteEncadenada(T nPrimero)
    {
        if(nPrimero == null)
        {
            throw new NullPointerException();
        }
        primero = new NodoListaSencilla<T>( nPrimero );
    }

    /**
     * Agrega un elemento al final de la lista
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elem el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
    public boolean add( T elem )throws NullPointerException
    {
        if(elem == null)
        {
            throw new NullPointerException( );
        }
        
        boolean agregado = false;
        if(primero == null)
        {
            primero = new NodoListaSencilla<T>( elem );
            agregado = true;
        }
        else
        {
            NodoListaSencilla<T> n = primero;
            boolean existe = false;
            while(  n.darSiguiente( ) != null && !existe)
            {
                if(n.darElemento( ).darIdentificador( ).equals( elem.darIdentificador( ) ))
                {
                    existe = true;
                }
                n = n.darSiguiente( );
            }
            if(!n.darElemento( ).darIdentificador( ).equals( elem.darIdentificador( ) ))
            {
                n.cambiarSiguiente( new NodoListaSencilla<T>( elem ) );
                agregado = true;
            }
            
        }
        
        return agregado;
    }

    /**
     * Agrega un elemento en la posici�n dada de la lista. Todos los elementos siguientes se desplazan
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param pos la posici�n donde se desea agregar. Si pos es igual al tama�o de la lista se agrega al final
     * @param elem el elemento que se desea agregar
     * @throws IndexOutOfBoundsException si el inidice es < 0 o > size()
     * @throws NullPointerException Si el elemento que se quiere agregar es null.
     */
    public void add( int pos, T elem )
    {
        if(elem == null)
        {
            throw new NullPointerException( );
        }
        NodoListaSencilla<T> nuevo = new NodoListaSencilla<T>( elem );
        
        if(!contains( elem ))
        {
            
            if(pos == 0)
            {
                nuevo.cambiarSiguiente( primero );
                primero = nuevo;
            }
            else
            {
                NodoListaSencilla<T> n = primero;
                int posActual = 0;
                while( posActual < (pos-1) && n != null )
                {
                    posActual++;
                    n = n.darSiguiente( );
                }
                if(posActual != (pos-1))
                {
                    throw new IndexOutOfBoundsException( );
                }
                nuevo.cambiarSiguiente( n.darSiguiente( ) );
                n.cambiarSiguiente( nuevo );
            }
        }
        
    }

    
    public ListIterator<T> listIterator( )
    {
        throw new UnsupportedOperationException ();
    }

    
    public ListIterator<T> listIterator( int pos )
    {
        throw new UnsupportedOperationException ();
    }

    /**
     * Elimina el nodo que contiene al objeto que llega por par�metro
     * @param objeto el objeto que se desea eliminar. objeto != null
     * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
     */
    public boolean remove( Object objeto )
    {
     // TODO Parte 3.A: Completar seg�n la documentaci�n
        
        int pos = indexOf( objeto );
        
        boolean cambio = false;
        
       
        
        if (pos == 0)
        {
            primero = primero.darSiguiente( );
            cambio= true;
        }
        else
        {
            NodoListaSencilla<T> anterior = getNodo(pos-1);
            NodoListaSencilla<T> actual = anterior.darSiguiente( );
            NodoListaSencilla<T> siguente = actual.darSiguiente( );
            
            anterior.cambiarSiguiente( siguente );
            cambio= true;
        }
        return cambio;
       
    }

    /**
     * Elimina el nodo en la posici�n por par�metro
     * @param pos la posici�n que se desea eliminar
     * @return el elemento eliminado
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
    public T remove( int pos )
    {
     // TODO Parte 3.A: Completar seg�n la documentaci�n
       
        
        if (pos == 0)
        {
            T borrado = primero.darElemento( );
            primero = primero.darSiguiente( );
            return borrado;
        }
        else
        {
            NodoListaSencilla<T> anterior = getNodo(pos-1);
            NodoListaSencilla<T> actual = anterior.darSiguiente( );
            NodoListaSencilla<T> siguente = actual.darSiguiente( );
            
            anterior.cambiarSiguiente( siguente );
            return actual.darElemento( );
        }
        
    }

    /**
     * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro
     * @param coleccion la colecci�n de elementos a mantener. coleccion != null
     * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
     */
    public boolean retainAll( Collection<?> coleccion )
    {
     // TODO Parte 3.A: Completar seg�n la documentaci�n
        
        boolean cambio = false;
        int tamano = size();
        
        if(primero!= null)
        {
            NodoListaSencilla<T> x = primero;
            
            while( x != null)
            {
                if(!coleccion.contains( primero.darElemento( ) ))
                {
                    primero = primero.darSiguiente( );
                }
                    
                NodoListaSencilla<T> ElElemento = x.darSiguiente();
                if(!coleccion.contains( ElElemento ))
                {
                    if ( ElElemento != null )
                    {
                        x.cambiarSiguiente( ElElemento.darSiguiente( ) );

                    }
                }
                x = x.darSiguiente( );
            }
        }

        if(size() < tamano)
        {
            cambio = true;
        }
      
        return cambio;
       
    }
    
    /**
     * Crea una lista con los elementos de la lista entre las posiciones dadas
     * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
     * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
     * @return una lista con los elementos entre las posiciones dadas
     * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
     */
    public List<T> subList( int inicio, int fin )
    {
     // TODO Parte 3.A: Completar seg�n la documentaci�n
        if (inicio < 0 || fin >= size())
        {
           throw new IndexOutOfBoundsException( );
        }
        
        ListaSencillamenteEncadenada<T> lista = new ListaSencillamenteEncadenada<T>( );
        NodoListaSencilla<T> x = ( NodoListaSencilla<T> )primero;
        int cuenta = -1;
        
        while (x != null)
        {
            cuenta++;
            if (cuenta >= inicio && cuenta<fin)
            {
                lista.add( x.darElemento( ) );
            }
            if (cuenta > fin)
            {
                break;
            }
            x = ( NodoListaSencilla<T> )x.darSiguiente( );
        }
        return lista;
    }
    
    public void invertir()
    {
        int tamano = size();
        
        while (tamano>1)
        {
            for(int i=0; i<tamano; i++)
            {
                if (i == 0)
                {
                    NodoListaSencilla<T> cambiar = primero;
                    NodoListaSencilla<T> siguiente2 = primero.darSiguiente( ).darSiguiente( );
                    
                    primero = primero.darSiguiente( );
                    primero.cambiarSiguiente( cambiar );
                    cambiar.cambiarSiguiente( siguiente2 );
                }
                
                NodoListaSencilla<T> actual = primero.darSiguiente( );
                NodoListaSencilla<T> siguiente = actual.darSiguiente( );
                NodoListaSencilla<T> siguiente2 = actual.darSiguiente( ).darSiguiente( );
                
                actual.cambiarSiguiente( siguiente2 );
                siguiente.cambiarSiguiente( actual );
            }
        }
    }
    
    public void invertir2()
    {
        NodoListaSencilla<T> invierte = primero;
        NodoListaSencilla<T> temp = null;

        while (invierte.darSiguiente() != null) {

             temp = invierte.darSiguiente();
    
             invierte.cambiarSiguiente( temp.darSiguiente( ) );
    
             temp.cambiarSiguiente(primero);
    
             primero = temp;
        }
    }

}
