package taller.estructuras;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Clase que representa una lista sencillamente encadenada
 * @author alvar-go
 * @param <T>
 */
public class Pila<T extends IdentificadoUnicamente> extends ListaEncadenadaAbstracta<T>
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Construye una lista vacia
     * post: se ha inicializado el primer nodo en null
     */
    public Pila()
    {
        primero = null;
    }
    
    /**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
    public Pila(T nPrimero)
    {
        if(nPrimero == null)
        {
            throw new NullPointerException();
        }
        primero = new NodoListaSencilla<T>( nPrimero );
    }

    /**
     * Agrega un elemento al comienzo de la lista
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elem el elemento que se desea agregar.
     * @throws NullPointerException si el elemento es nulo
     */
    public void push( T elem )throws NullPointerException
    {
        if(elem == null)
        {
            throw new NullPointerException( );
        }
        
        if(primero == null)
        {
            primero = new NodoListaSencilla<T>( elem );

        }
        else
        {
        	NodoListaSencilla<T> nuevo = new NodoListaSencilla<T>( elem );
        	
            NodoListaSencilla<T> n = primero;
            
            primero = nuevo;
            
            primero.cambiarSiguiente(n);
        }
    }

    /**
     * Elimina el nodo en la primera posicion 
     * @return el elemento eliminado
     */
    public T pop()
    {
    	T borrado = primero.darElemento( );
        primero = primero.darSiguiente( );
        return borrado;
        
    }
    
    public T top()
    {
    	if(primero == null)
    	{
    		return null;
    	}
    	return primero.darElemento();
    }
    
    public NodoListaSencilla<T> darPrimerNodo()
    {
    	return primero;
    }
    	

	public boolean add(T e) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	public void add(int index, T element) {
		// TODO Auto-generated method stub
		
	}

	public T remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	public ListIterator<T> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<T> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}
}
