package taller.estructuras;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Clase que representa una lista sencillamente encadenada
 * @author alvar-go
 * @param <T>
 */
public class Cola<T extends IdentificadoUnicamente> extends ListaEncadenadaAbstracta<T>
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private int tamano;

    /**
     * Construye una lista vacia
     * post: se ha inicializado el primer nodo en null
     */
    public Cola()
    {
        primero = null;
        tamano = 0;
    }
    
    /**
     * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
    public Cola(T nPrimero)
    {
        if(nPrimero == null)
        {
            throw new NullPointerException();
        }
        primero = new NodoListaSencilla<T>( nPrimero );
        ultimo = new NodoListaSencilla<T>( nPrimero );
        tamano = 0;
    }

    /**
     * Agrega un elemento al final de la lista
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elem el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
    public void enqueue( T elem )throws NullPointerException
    {
        
    	if(elem == null)
        {
            throw new NullPointerException( );
        }

        NodoListaSencilla<T> nuevo = new NodoListaSencilla<T>( elem );
        
        if(primero == null)
        {
            primero = nuevo;
            ultimo = nuevo;
            tamano+=1;
        }
        else
        {
            ultimo.cambiarSiguiente(nuevo);
            ultimo = nuevo;
            tamano+=1;
        }
    }


    /**
     * Elimina el nodo en la primera posicion 
     * @return el elemento eliminado
     */
    public T dequeue( )
    {
    	
    	T borrado = primero.darElemento( );
        primero = primero.darSiguiente( );
        tamano-=1;
        return borrado;
        
    }

    
    public T first()
    {
    	return primero.darElemento();
    }
    
    public int darSize()
    {
    	return tamano;
    }
    
	public boolean add(T e) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	public void add(int index, T element) {
		// TODO Auto-generated method stub
		
	}

	public T remove(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public ListIterator<T> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	public ListIterator<T> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<T> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}

}
